const laptopsElement = document.getElementById("laptops");
const laptopPriceElement = document.getElementById("name");
let specsElement = document.getElementById("specs");

laptops=[];


fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops))


const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addToLaptopMenu(x));
    specsElement.innerText = laptops[0].specs;
}

const addToLaptopMenu = (laptop) => {
    const laptopElement = document.createElement("option") //Look into how the html file can be updated when first seeing the document and add the first laptop specs
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title))
    laptopsElement.appendChild(laptopElement)

    //console.log(typeof value)

}

const handleLaptopChange = e =>{
    const selectedLaptop = laptops[e.target.selectedIndex]
    specsElement.innerText = selectedLaptop.specs // This array should be manipulated, got this far. next step is to loop through the specs and printing out relevent info, instead of entire array
    //console.log(selectedLaptop)
}

laptopsElement.addEventListener("change", handleLaptopChange);



