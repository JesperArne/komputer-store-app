General Information:
    This is an assignment project. The project takes a user input through a HTML interface and handles that input using Javascript. User can currently Work, transfere the salary to a bank and take a loan at the bank.

AUTHORS
    Jesper Arne Erne Jensen - arneerne@gmail.com

THANKS
    Unknown author of "Part 4 - Retrieving data from an API.mp4" to understand how an API works

INSTALL
    Open the folder content in Visual Studio Code. Either Run index.html on a live server of choice. alternativly open the index.html file with preffered browser.

LICENSE
    Non

BUGS
    This webpage is still a WIP - feature of buying a selected laptop is still missing. Styling is not finished.

FURTHER WORK
    1:look at comment in laptop.js file(line 32) to get specific info